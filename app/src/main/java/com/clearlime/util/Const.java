package com.clearlime.util;

/**
 * Created by user on 13.06.2015.
 */
public class Const {
    public interface GuideFragmentParams {
        String TITLE = "TITLE";
        String TEXT = "TEXT";
        String IMAGE = "IMAGE";
        String TYPE = "TYPE";
    }

    public interface WebFragmentParams {
        String URL = "URL";
        String SECTION = "SECTION";
    }

    public static final String PREFS = "PREFS";
    public static final String FIRST_RUN = "FIRST_RUN";
    public static final int LOGOUT = 4;
    public static final int MYAUTO = 1;
}
