package com.clearlime.ui.activity;

import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.GestureDetector;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;

import com.clearlime.R;
import com.clearlime.ui.fragment.ContainerFragment;
import com.clearlime.ui.fragment.SplashFragment;
import com.clearlime.ui.fragment.WebViewFragment;
import com.clearlime.util.ConnectionManager;
import com.clearlime.util.Const;
import com.mikepenz.iconics.typeface.FontAwesome;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.DividerDrawerItem;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by user on 13.06.2015.
 */
public class MainActivity extends AppCompatActivity {
    @InjectView(R.id.toolbar) Toolbar mToolbar;
    private ActionBarDrawerToggle mDrawerToggle;
    private static final int HEADER = 0;
    private static GestureDetector mGestureDetector;
    private static String[] mDrawerTitles;
    public static String[] mDrawerUrls;
    private static int[] mDrawerIcons = new int[] {
            R.drawable.ic_add,
            R.drawable.ic_myauto,
            R.drawable.ic_profile,
            R.drawable.ic_tariff,
            R.drawable.ic_icon_about,
            R.drawable.ic_icon_logout,
    };
    private Drawer mDrawer;

    public Toolbar getToolbar() {
        return mToolbar;
    }

    public Drawer getDrawer() {
        return mDrawer;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mDrawerTitles = getResources().getStringArray(R.array.drawer_items_titles);
        mDrawerUrls = getResources().getStringArray(R.array.urls);
        setContentView(R.layout.main_activity);
        ButterKnife.inject(this);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        /*mDrawerList.setAdapter(new DrawerAdapter());
        mDrawerList.setLayoutManager(new LinearLayoutManager(this));
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawer, mToolbar, R.string.app_name, R.string.app_name){
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }
        };
        mDrawer.setDrawerListener(mDrawerToggle);
        mGestureDetector = new GestureDetector(MainActivity.this, new GestureDetector.SimpleOnGestureListener() {
            @Override public boolean onSingleTapUp(MotionEvent e) {
                return true;
            }
        });*/
        final Typeface reg = Typeface.createFromAsset(getAssets(), "MyriadProRegular.ttf"),
                 bold = Typeface.createFromAsset(getAssets(), "MyriadProBold.ttf");

        mDrawer = new DrawerBuilder()
                .withActivity(this)
                .withToolbar(mToolbar)
                .withActionBarDrawerToggle(true)
                .withHeaderDivider(true)
                .withHeader(R.layout.drawer_header)
                .withTranslucentStatusBar(true)
                .withSliderBackgroundColorRes(R.color.company_color)
                .addDrawerItems(
                        new PrimaryDrawerItem().withName(mDrawerTitles[0])
                                .withIcon(getResources().getDrawable(mDrawerIcons[0]))
                                .withTextColorRes(R.color.trans_white).withTypeface(reg).withIdentifier(0).withSelectedColorRes(R.color.company_color).withSelectedTextColor(Color.WHITE),
                        new DividerDrawerItem(),
                        new PrimaryDrawerItem().withName(mDrawerTitles[1])
                                .withIcon(getResources().getDrawable(mDrawerIcons[1]))
                                .withTextColorRes(R.color.trans_white).withTypeface(reg).withIdentifier(1).withSelectedColorRes(R.color.company_color).withSelectedTextColor(Color.WHITE),
                        new DividerDrawerItem(),
                        new PrimaryDrawerItem().withName(mDrawerTitles[2])
                                .withIcon(getResources().getDrawable(mDrawerIcons[2]))
                                .withTextColorRes(R.color.trans_white).withTypeface(reg).withIdentifier(2).withSelectedColorRes(R.color.company_color).withSelectedTextColor(Color.WHITE),
                        new DividerDrawerItem(),
                        new PrimaryDrawerItem().withName(mDrawerTitles[3])
                                .withIcon(getResources().getDrawable(mDrawerIcons[3]))
                                .withTextColorRes(R.color.trans_white).withTypeface(reg).withIdentifier(3).withSelectedColorRes(R.color.company_color).withSelectedTextColor(Color.WHITE),
                        new DividerDrawerItem(),
                        new PrimaryDrawerItem().withName(mDrawerTitles[4])
                                .withIcon(getResources().getDrawable(mDrawerIcons[4]))
                                .withTextColorRes(R.color.trans_white).withTypeface(reg).withIdentifier(4).withSelectedColorRes(R.color.company_color).withSelectedTextColor(Color.WHITE),
                        new DividerDrawerItem(),
                        new PrimaryDrawerItem().withName(mDrawerTitles[5])
                                .withIcon(getResources().getDrawable(mDrawerIcons[5]))
                                .withTextColorRes(R.color.trans_white).withTypeface(reg).withIdentifier(5).withSelectedColorRes(R.color.company_color).withSelectedTextColor(Color.WHITE)
                )
                .withOnDrawerListener(new Drawer.OnDrawerListener() {
                    @Override
                    public void onDrawerOpened(View view) {
                        mDrawer.getDrawerLayout().setDrawerShadow(R.drawable.shadow, Gravity.RIGHT);
                    }

                    @Override
                    public void onDrawerClosed(View view) {

                    }

                    @Override
                    public void onDrawerSlide(View view, float v) {

                    }
                })
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public boolean onItemClick(AdapterView<?> adapterView, View view, int ind, long l, IDrawerItem iDrawerItem) {
                        int i = iDrawerItem.getIdentifier();
                        Bundle args = new Bundle();
                        Fragment fr = new WebViewFragment();
                        args.putString(Const.WebFragmentParams.URL, mDrawerUrls[i]);
                        args.putInt(Const.WebFragmentParams.SECTION, i);
                        fr.setArguments(args);
                        commitFragment(fr);
                        mToolbar.setTitle(getResources().getStringArray(R.array.titles)[i]);
                        mDrawer.closeDrawer();
                        ArrayList<IDrawerItem> items =  mDrawer.getDrawerItems();
                        for (IDrawerItem item : items) {
                            if (item.getIdentifier() >= 0 && item.getIdentifier() <= 4 && item.getIdentifier() != i) {
                                ((PrimaryDrawerItem) item).setTypeface(reg);
                            }
                            if (item.getIdentifier() == i) {
                                ((PrimaryDrawerItem) item).setTypeface(bold);
                            }
                        }

                        return true;
                    }
                }).build();



        /*mDrawerList.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {
            @Override
            public boolean onInterceptTouchEvent(RecyclerView recyclerView, MotionEvent motionEvent) {
                View child = recyclerView.findChildViewUnder(motionEvent.getX(), motionEvent.getY());
                if (null != child && mGestureDetector.onTouchEvent(motionEvent)) {
                    int index = recyclerView.getChildPosition(child) - 1;
                    mDrawer.closeDrawers();
                    Bundle args = new Bundle();
                    Fragment fr = new WebViewFragment();
                    args.putString(Const.WebFragmentParams.URL, mDrawerUrls[index]);
                    args.putInt(Const.WebFragmentParams.SECTION, index);
                    fr.setArguments(args);
                    commitFragment(fr);
                    mToolbar.setTitle(getResources().getStringArray(R.array.titles)[index]);
                    mDrawer.closeDrawers();

                    return true;
                }

                return false;
            }

            @Override
            public void onTouchEvent(RecyclerView recyclerView, MotionEvent motionEvent) {
            }
        });*/
        commitFragment(new SplashFragment());
    }

    @Override
    public void onBackPressed() {
        if (mDrawer.isDrawerOpen()) {
            mDrawer.closeDrawer();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    public void commitFragment(Fragment fragment) {
        if (fragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();
        }
    }

    private class DrawerAdapter extends RecyclerView.Adapter<DrawerAdapter.ViewHolder> {
        private final int[] mIcons = new int[] {
                R.drawable.ic_add,
                R.drawable.ic_myauto,
                R.drawable.ic_profile,
                R.drawable.ic_icon_about,
                R.drawable.ic_icon_logout
        };

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            return HEADER != i ? new ViewHolder( LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.drawer_item, viewGroup, false), i)
                    : new ViewHolder( LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.drawer_header, viewGroup, false), i);
        }

        @Override
        public void onBindViewHolder(ViewHolder viewHolder, int i) {
            if (HEADER == viewHolder.mType) {
                return;
            }
            viewHolder.mText.setText(getApplicationContext().getResources().getStringArray(R.array.drawer_items_titles)[i - 1]);
            viewHolder.mIcon.setImageResource(mIcons[i - 1]);
        }

        @Override
        public int getItemCount() {
            return mIcons.length + 1;
        }

        @Override
        public int getItemViewType(int position) {
            return position;
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            public int mType;
            public ImageView mIcon;
            public TextView mText;

            public ViewHolder(View itemView, int type) {
                super(itemView);
                mType = type;
                if (HEADER == type) {
                    return;
                }
                mIcon = (ImageView) itemView.findViewById(R.id.icon);
                mText = (TextView) itemView.findViewById(R.id.text);
                mText.setTypeface( Typeface.createFromAsset(getApplicationContext().getAssets(), "MyriadProRegular.ttf") );
            }
        }
    }
}
