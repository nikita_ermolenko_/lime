package com.clearlime.ui.activity;


import android.app.Activity;
import android.graphics.drawable.TransitionDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.clearlime.util.ConnectionManager;
import com.clearlime.R;


public class NotMainActivity extends Activity {
    private RelativeLayout mSplash;
    private RelativeLayout mNoConnectionView;
    private ImageView mLogo;
    private WebView mWebView;
    private ConnectionManager mConnectionManager;
    private static final String mUrl = "http://clearlime.ru/m/worker/";
    private int HANDLER_INTERVAL = 1500;
    private int START_INTERVAL = 4000;
    private CookieManager mCookieManager;

    private Handler mStartHandler = new Handler();

    private Runnable mStartRunnable = new Runnable() {
        @Override
        public void run() {
            mSplash.setVisibility(View.GONE);
            if (!mConnectionManager.hasInternetConnection()) {
                mWebView.setVisibility(View.GONE);
            }
        }
    };

    private  Handler mTransitionHandler = new Handler();

    private Runnable mTransitionRunnable = new Runnable() {
        @Override
        public void run() {
            TransitionDrawable transition = (TransitionDrawable) getResources()
                    .getDrawable(R.drawable.second_transition);
            transition.startTransition(HANDLER_INTERVAL);
            mLogo.setImageDrawable(transition);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActionBar().hide();
        setContentView(R.layout.activity_main);
        mConnectionManager = new ConnectionManager(this);
        mSplash = (RelativeLayout) findViewById(R.id.splash);
        mLogo = (ImageView) findViewById(R.id.logo);
        mWebView = (WebView) findViewById(R.id.webView);
        mNoConnectionView = (RelativeLayout)  findViewById(R.id.no_connection);
        TransitionDrawable transition = (TransitionDrawable) getResources()
                .getDrawable(R.drawable.first_transition);
        transition.startTransition(HANDLER_INTERVAL);
        mLogo.setImageDrawable(transition);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        delayedTransition(HANDLER_INTERVAL);
        delayedStart(START_INTERVAL);
        if (mConnectionManager.hasInternetConnection()) {
            mCookieManager = CookieManager.getInstance();
            mCookieManager.setAcceptCookie(true);
            mWebView.loadUrl(mUrl);
            WebSettings webSettings = mWebView.getSettings();
            webSettings.setJavaScriptEnabled(true);
            mWebView.setWebChromeClient(new WebChromeClient());
            mWebView.setWebViewClient(new WebViewClient());
            mWebView.loadUrl(mUrl);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        mConnectionManager = null;
    }

    @Override
    protected void onResume() {
        super.onResume();
        mConnectionManager = new ConnectionManager(this);
    }

    @Override
    public void onBackPressed() {
        if(mWebView.canGoBack()) {
            mWebView.goBack();
        } else {
            super.onBackPressed();
        }
    }

    private void delayedTransition(int delayMillis) {
        mTransitionHandler.removeCallbacks(mTransitionRunnable);
        mTransitionHandler.postDelayed(mTransitionRunnable, delayMillis);
    }

    private void delayedStart(int delayMillis) {
        mStartHandler.removeCallbacks(mStartRunnable);
        mStartHandler.postDelayed(mStartRunnable, delayMillis);
    }
}
