package com.clearlime.ui.fragment;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.TransitionDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.CookieManager;
import android.widget.ImageView;

import com.clearlime.R;
import com.clearlime.ui.activity.MainActivity;
import com.clearlime.util.Const;

import java.math.MathContext;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by user on 13.06.2015.
 */
public class SplashFragment extends Fragment {
    @InjectView(R.id.logo) ImageView mLogo;
    private int HANDLER_INTERVAL = 1500;
    private int START_INTERVAL = 4000;
    private MainActivity mContext;

    private Handler mStartHandler = new Handler();

    private Runnable mStartRunnable = new Runnable() {
        @Override
        public void run() {
            Fragment fr = new ContainerFragment();
            if (!mContext.getSharedPreferences(Const.PREFS, Context.MODE_PRIVATE).getBoolean(Const.FIRST_RUN, true)) {
                Bundle args = new Bundle();
                fr = new WebViewFragment();
                args.putString(Const.WebFragmentParams.URL, mContext.mDrawerUrls[0]);
                args.putInt(Const.WebFragmentParams.SECTION, 0);
                fr.setArguments(args);
                mContext.getToolbar().setTitle(getResources().getStringArray(R.array.titles)[0]);
            }
            mContext.commitFragment(fr);
        }
    };

    private  Handler mTransitionHandler = new Handler();

    private Runnable mTransitionRunnable = new Runnable() {
        @Override
        public void run() {
            TransitionDrawable transition = (TransitionDrawable) getResources()
                    .getDrawable(R.drawable.second_transition);
            transition.startTransition(HANDLER_INTERVAL);
            mLogo.setImageDrawable(transition);
        }
    };

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mContext = (MainActivity) activity;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.splash_fragment, container, false);
        ButterKnife.inject(this, view);
        mContext.getSupportActionBar().hide();
        DrawerLayout drawerLayout = mContext.getDrawer().getDrawerLayout();
        drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);

        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        TransitionDrawable transition = (TransitionDrawable) getResources()
                .getDrawable(R.drawable.first_transition);
        transition.startTransition(HANDLER_INTERVAL);
        mLogo.setImageDrawable(transition);
        delayedTransition(HANDLER_INTERVAL);
        delayedStart(START_INTERVAL);
    }



    @Override
    public void onDetach() {
        super.onDetach();
        mContext = null;
    }

    private void delayedTransition(int delayMillis) {
        mTransitionHandler.removeCallbacks(mTransitionRunnable);
        mTransitionHandler.postDelayed(mTransitionRunnable, delayMillis);
    }

    private void delayedStart(int delayMillis) {
        mStartHandler.removeCallbacks(mStartRunnable);
        mStartHandler.postDelayed(mStartRunnable, delayMillis);
    }
}
