package com.clearlime.ui.fragment;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.clearlime.R;
import com.clearlime.ui.activity.MainActivity;
import com.clearlime.util.Const;
import com.viewpagerindicator.CirclePageIndicator;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

/**
 * Created by user on 13.06.2015.
 */
public class ContainerFragment extends Fragment {
    @InjectView(R.id.view_pager) ViewPager mPager;
    @InjectView(R.id.continue_btn) TextView mContinueBtn;
    @InjectView(R.id.next_btn) TextView mNextBtn;
    @InjectView(R.id.indicator) CirclePageIndicator mIndicator;
    private static final int PAGES_NUM = 3;
    private TextView[] mDots;
    private MainActivity mContext;

    public TextView getmNextBtn() {
        return mNextBtn;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mContext = null;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mContext = (MainActivity) activity;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.container_fragment, container, false);
        ActionBar actionBar = mContext.getSupportActionBar();
        ButterKnife.inject(this, view);
        if (null != actionBar) {
            actionBar.hide();
        }
        DrawerLayout drawerLayout = mContext.getDrawer().getDrawerLayout();
        drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);

        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Typeface face = Typeface.createFromAsset(mContext.getAssets(), "MyriadProCondRegular.ttf");
        mContinueBtn.setTypeface(face);
        mNextBtn.setTypeface(face);
        mPager.setAdapter(new ViewPagerAdapter(getChildFragmentManager()));
        mPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (2 == position) {
                    mNextBtn.setText(R.string.close);
                } else {
                    mNextBtn.setText(R.string.next_text);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        mIndicator.setViewPager(mPager, 0);
        mContext.getSharedPreferences(Const.PREFS, Context.MODE_PRIVATE).edit().putBoolean(Const.FIRST_RUN, false).apply();
        //mContext.getDrawer().setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
    }

    private class ViewPagerAdapter extends FragmentPagerAdapter {
        public ViewPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            Fragment fr = new GuideFragment();
            Bundle args = new Bundle();
            switch (position) {
                case 0:
                    args.putInt(Const.GuideFragmentParams.TITLE, R.string.first_title);
                    args.putInt(Const.GuideFragmentParams.TEXT, R.string.first_text);
                    args.putInt(Const.GuideFragmentParams.IMAGE, R.drawable.ic_1);
                    break;
                case 1:
                    args.putInt(Const.GuideFragmentParams.TITLE, R.string.second_title);
                    args.putInt(Const.GuideFragmentParams.TEXT, R.string.second_text);
                    args.putInt(Const.GuideFragmentParams.IMAGE, R.drawable.ic_2);
                    break;
                case 2:
                    args.putInt(Const.GuideFragmentParams.TITLE, R.string.third_title);
                    args.putInt(Const.GuideFragmentParams.TEXT, R.string.third_text);
                    args.putInt(Const.GuideFragmentParams.IMAGE, R.drawable.ic_3);
                    break;
            }
            fr.setArguments(args);

            return fr;
        }

        @Override
        public int getCount() {
            return PAGES_NUM;
        }
    }

    @OnClick(R.id.continue_btn)
    public void onContinueClick(View v) {
        Bundle args = new Bundle();
        args.putString(Const.WebFragmentParams.URL, "http://clearlime.ru/m/customer/index.php?mode=order");
        Fragment fr = new WebViewFragment();
        fr.setArguments(args);
        mContext.commitFragment(fr);
    }

    @OnClick(R.id.next_btn)
    public void onNextClick(View v) {
        if (TextUtils.equals(mNextBtn.getText(), getString(R.string.close))) {
            Bundle args = new Bundle();
            args.putString(Const.WebFragmentParams.URL, "http://clearlime.ru/m/customer/index.php?mode=order");
            Fragment fr = new WebViewFragment();
            fr.setArguments(args);
            mContext.commitFragment(fr);
        }
        mPager.setCurrentItem((mPager.getCurrentItem() + 1) % PAGES_NUM);
    }

}
