package com.clearlime.ui.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.view.Window;
import android.webkit.CookieManager;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import com.clearlime.R;
import com.clearlime.ui.activity.MainActivity;
import com.clearlime.util.ConnectionManager;
import com.clearlime.util.Const;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by user on 13.06.2015.
 */
public class WebViewFragment extends Fragment {
    @InjectView(R.id.web_view) WebView mWebView;
    @InjectView(R.id.stub_import) ViewStub mProgress;
    private MainActivity mContext;
    private String mUrl;
    private int mSection;
    private CookieManager mCookieManager;
    private ConnectionManager mConnectionManager;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mUrl = getArguments().getString(Const.WebFragmentParams.URL);
        mSection = getArguments().getInt(Const.WebFragmentParams.SECTION);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.web_fragment, container, false);
        ButterKnife.inject(this, view);
        DrawerLayout drawerLayout = mContext.getDrawer().getDrawerLayout();
        drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        if (Const.LOGOUT != mSection) {
            setHasOptionsMenu(true);
        }
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //mContext.getDrawer().setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
        ActionBar bar = mContext.getSupportActionBar();
        if (null != bar) {
            bar.show();
        }
        if (mConnectionManager.hasInternetConnection()) {
            mCookieManager = CookieManager.getInstance();
            mCookieManager.setAcceptCookie(true);
            mWebView.loadUrl(mUrl);
            WebSettings webSettings = mWebView.getSettings();
            webSettings.setJavaScriptEnabled(true);
            mWebView.setWebChromeClient(new WebChromeClient() {
                public void onProgressChanged(WebView view, int progress) {
                    if (100 == progress) {
                        mProgress.setVisibility(View.GONE);
                    } else {
                        mProgress.setVisibility(View.VISIBLE);
                    }
                }
            });
            mWebView.setWebViewClient(new WebViewClient());
            mWebView.loadUrl(mUrl);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mContext = (MainActivity) activity;
        mConnectionManager = new ConnectionManager(mContext);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mContext = null;
        mConnectionManager = null;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        if (Const.MYAUTO == mSection) {
            inflater.inflate(R.menu.add_auto, menu);
        } else {
            inflater.inflate(R.menu.actions, menu);
        }
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.add:
                if (Const.MYAUTO == mSection) {
                    mWebView.loadUrl("http://clearlime.ru/m/customer/index.php?mode=addauto");
                } else {
                    mWebView.loadUrl("http://clearlime.ru/m/customer/index.php?mode=order");
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
